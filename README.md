# Instalasi PostgreSQL di Komputer
Berikut adalah langkah-langkah instalasi PostgreSQL pada komputer Anda:

1. Kunjungi situs web resmi PostgreSQL di https://www.postgresql.org/ dan pilih versi yang sesuai dengan sistem operasi Anda (Windows, macOS, atau Linux) atau anda bisa mendownloadnya secara langsung melalui link berikut : [Link Download](https://www.postgresql.org/download/)
2. Unduh paket instalasi PostgreSQL yang sesuai untuk sistem operasi Anda.
3. Buka paket instalasi yang telah diunduh dan ikuti instruksi pada layar untuk menginstal PostgreSQL.
4. Selama proses instalasi, Anda akan diminta untuk memilih komponen yang ingin diinstal. 
5. Pastikan Anda memilih "PostgreSQL Server" atau opsi yang serupa yang menyertakan server database.
6. Lanjutkan dengan pengaturan konfigurasi yang diminta selama instalasi. Anda akan diminta untuk memasukkan kata sandi untuk pengguna superuser (biasanya "postgres"). Ingatlah kata sandi ini karena akan digunakan untuk mengakses server PostgreSQL.
7. Setelah instalasi selesai, lanjutkan dengan mengonfigurasi server PostgreSQL. Ini dapat dilakukan melalui alat administrasi yang disertakan seperti pgAdmin atau melalui baris perintah.
8. Pastikan server PostgreSQL sudah berjalan dengan baik dengan menguji koneksi ke server menggunakan alat administrasi atau perintah seperti psql.
9. Jika koneksi berhasil, Anda sekarang memiliki instalasi PostgreSQL yang berjalan di komputer Anda.
Setelah instalasi selesai, Anda dapat menggunakan alat-alat seperti DBeaver atau pgAdmin untuk terhubung dan bekerja dengan server PostgreSQL. 
10. Pastikan untuk menggunakan informasi koneksi yang benar seperti host, port, database, username, dan password saat mengatur koneksi ke server PostgreSQL.

[Lihat Selengkapnya disini](https://www.nesabamedia.com/cara-install-postgresql-di-windows-10/)

# Instalasi PostgreSQL di DBeaver
### Persyaratan
DBeaver telah terinstal di komputer Anda. 
Jika belum, Anda dapat mengunduhnya dari situs web resmi DBeaver.

Langkah-langkah :


1. Buka DBeaver di komputer Anda.

2. Pilih menu "Database" di toolbar atas, kemudian klik "New Database Connection".
3. Pada jendela "New Connection Wizard", pilih "PostgreSQL" dari daftar database yang tersedia. Klik "Next".
4. Masukkan informasi koneksi ke server PostgreSQL:
    - Host: (Alamat IP atau hostname dari server PostgreSQL)
    - Port: (Port yang digunakan oleh server PostgreSQL, biasanya 5432)
    - Database: (Nama database yang ingin Anda hubungkan)
    - Username: (Nama pengguna untuk koneksi ke server PostgreSQL)
    - Password: (Kata sandi untuk koneksi ke server PostgreSQL)

5. Klik "Test Connection" untuk memverifikasi koneksi ke server PostgreSQL.
6. Jika koneksi berhasil, klik "Finish" untuk menyelesaikan proses pengaturan koneksi.
7. Koneksi PostgreSQL sekarang akan ditambahkan ke daftar koneksi di DBeaver.
8. Anda dapat mengklik dua kali pada koneksi tersebut untuk terhubung ke server PostgreSQL dan mulai bekerja dengan database.

Dengan mengikuti langkah-langkah di atas, Anda sekarang dapat menginstal dan mengkonfigurasi koneksi PostgreSQL di DBeaver. Pastikan Anda memiliki informasi yang benar tentang host, port, database, username, dan password untuk dapat terhubung ke server PostgreSQL dengan sukses.

